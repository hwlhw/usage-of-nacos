package com.example;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;

import java.util.Properties;

public class Test {
    public static void main(String[] args) throws NacosException {
        // nacos的访问地址
        String serverAddr = "localhost:8848";
        // data id
        String dataId = "test.properties";
        // group
        String group = "DEFAULT_GROUP";
        // 创建properties属性，作用是将dataId和group两个属性放进去，先创建出来
        Properties properties = new Properties();
        // 指定nacos地址添加到properties属性中
        properties.put("serverAddr", serverAddr);
        // 获取配置
        ConfigService configService = NacosFactory.createConfigService(properties);
        // 传递的三个属性：dataId、group、超时时间
        String config = configService.getConfig(dataId, group, 5000);
        System.out.println("配置信息：" + config);
    }
}
